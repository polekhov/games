﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace game_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int n = 20;
        List<string> chance = new List<string>() {
                "Бесспорно",
                "Предрешено",
                "Никаких сомнений",
                "Определённо да",
                "Можешь быть уверен в этом",
                "Мне кажется — «да»",
                "Вероятнее всего",
                "Хорошие перспективы",
                "Знаки говорят — «да»",
                "Да",
                "Пока не ясно, попробуй снова",
                "Спроси позже",
                "Лучше не рассказывать",
                "Сейчас нельзя предсказать",
                "Сконцентрируйся и спроси опять",
                "Даже не думай",
                "Нет",
                "По моим данным — «нет»",
                "Перспективы не очень хорошие",
                "Весьма сомнительно"};

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            chance.Add(textBox1.Text);
            n++;
            listBox.Items.Add(textBox1.Text);
            textBox1.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int l = rnd.Next(0, n);
            label1.Text = chance[l];
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listBox.Items.Clear();
            chance.RemoveRange(20, n - 20);
            n = 20;
        }
    }
}
