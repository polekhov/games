﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace игра2
{
    public partial class Form1 : Form
    { 
        int n = 20;
        int m = 20;
        int[,] field;

        int playerPosI = 9;
        int playerPosJ = 1;

        int score = 0;
        int gameTime = 60;
        int currentTime = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void draw() {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    Panel panel = (Panel)tableLayoutPanel1.GetControlFromPosition(j, i);
                    switch (field[i, j]) {
                        case 1:
                            panel.BackColor = Color.Blue;
                            break;
                        case 2:
                            panel.BackColor = Color.Gold;
                            break;
                        default:
                            panel.BackColor = Color.White;
                            break;
                    }
                    
                }
            }

            ((Panel)tableLayoutPanel1.GetControlFromPosition(playerPosJ, playerPosI)).BackColor = Color.Cyan;

            scoreLabel.Text = "Ваш счёт: " + score;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            field = new int[n, m];
            field[0, 0] = 1;
            field[0, 1] = 1;
            field[0, 2] = 1;
            field[0, 3] = 1;
            field[0, 4] = 1;
            field[0, 5] = 1;
            field[0, 6] = 1;
            field[0, 7] = 1;
            field[0, 8] = 1;
            field[0, 9] = 1;
            field[0, 10] = 1;
            field[0, 11] = 1;
            field[0, 12] = 1;
            field[0, 13] = 1;
            field[0, 14] = 1;
            field[0, 15] = 1;
            field[0, 16] = 1;
            field[0, 17] = 1;
            field[0, 18] = 1;
            field[0, 19] = 1;


            field[1, 0] = 1;
            field[2, 0] = 1;
            field[3, 0] = 1;
            field[4, 0] = 1;
            field[5, 0] = 1;
            field[6, 0] = 1;
            field[7, 0] = 1;
            field[8, 0] = 1;
            field[9, 0] = 1;
            field[10, 0] = 1;
            field[11, 0] = 1;
            field[12, 0] = 1;
            field[13, 0] = 1;
            field[14, 0] = 1;
            field[15, 0] = 1;
            field[16, 0] = 1;
            field[17, 0] = 1;
            field[18, 0] = 1;

            field[19, 0] = 1;
            field[19, 1] = 1;
            field[19, 2] = 1;
            field[19, 3] = 1;
            field[19, 4] = 1;
            field[19, 5] = 1;
            field[19, 6] = 1;
            field[19, 7] = 1;
            field[19, 8] = 1;
            field[19, 9] = 1;
            field[19, 10] = 1;
            field[19, 11] = 1;
            field[19, 12] = 1;
            field[19, 13] = 1;
            field[19, 14] = 1;
            field[19, 15] = 1;
            field[19, 16] = 1;
            field[19, 17] = 1;
            field[19, 18] = 1;
            field[19, 19] = 1;

            field[1, 19] = 1;
            field[2, 19] = 1;
            field[5, 19] = 1;
            field[3, 19] = 1;
            field[4, 19] = 1;
            field[6, 19] = 1;
            field[7, 19] = 1;
            field[8, 19] = 1;
            field[9, 19] = 1;
            field[11, 19] = 1;
            field[12, 19] = 1;
            field[13, 19] = 1;
            field[14, 19] = 1;
            field[15, 19] = 1;
            field[16, 19] = 1;
            field[17, 19] = 1;
            field[18, 19] = 1;


            field[1, 4] = 1;
            field[1, 12] = 1;
            field[1, 17] = 1;

            field[2, 2] = 1;
            field[2, 3] = 1;
            field[2, 4] = 1;
            field[2, 6] = 1;
            field[2, 7] = 1;
            field[2, 8] = 1;
            field[2, 10] = 1;
            field[2, 12] = 1;
            field[2, 13] = 1;
            field[2, 14] = 1;
            field[2, 15] = 1;
            field[2, 17] = 1;

            field[3, 6] = 1;
            field[3, 10] = 1;
            field[3, 15] = 1;
            field[3, 17] = 1;

            field[4, 1] = 1;
            field[4, 2] = 1;
            field[4, 3] = 1;
            field[4, 4] = 1;
            field[4, 5] = 1;
            field[4, 6] = 1;
            field[4, 8] = 1;
            field[4, 9] = 1;
            field[4, 10] = 1;
            field[4, 11] = 1;
            field[4, 12] = 1;
            field[4, 13] = 1;

            field[5, 4] = 1;
            field[5, 8] = 1;
            field[5, 13] = 1;
            field[5, 14] = 1;
            field[5, 15] = 1;
            field[5, 16] = 1;
            field[5, 17] = 1;


            field[6, 2] = 1;
            field[6, 4] = 1;
            field[6, 6] = 1;
            field[6, 7] = 1;
            field[6, 8] = 1;
            field[6, 10] = 1;
            field[6, 11] = 1;
            field[6, 15] = 1;

            field[7, 2] = 1;
            field[7, 6] = 1;
            field[7, 11] = 1;
            field[7, 12] = 1;
            field[7, 13] = 1;
            field[7, 15] = 1;
            field[7, 17] = 1;
            field[7, 18] = 1;

            field[8, 2] = 1;
            field[8, 3] = 1;
            field[8, 4] = 1;
            field[8, 5] = 1;
            field[8, 6] = 1;
            field[8, 8] = 1;
            field[8, 9] = 1;
            field[8, 10] = 1;
            field[8, 11] = 1;
            field[8, 15] = 1;

            field[9, 2] = 1;
            field[9, 11] = 1;
            field[9, 13] = 1;
            field[9, 14] = 1;
            field[9, 15] = 1;
            field[9, 16] = 1;
            field[9, 17] = 1;

            field[10, 1] = 1;
            field[10, 2] = 1;
            field[10, 4] = 1;
            field[10, 5] = 1;
            field[10, 6] = 1;
            field[10, 7] = 1;
            field[10, 8] = 1;
            field[10, 11] = 1;
            field[10, 13] = 1;
            field[10, 17] = 1;

            field[11, 8] = 1;
            field[11, 9] = 1;
            field[11, 10] = 1;
            field[11, 11] = 1;
            field[11, 13] = 1;
            field[11, 15] = 1;
            field[11, 16] = 1;
            field[11, 17] = 1;

            field[12, 2] = 1;
            field[12, 3] = 1;
            field[12, 4] = 1;
            field[12, 5] = 1;
            field[12, 6] = 1;
            field[12, 11] = 1;
            field[12, 13] = 1;
            field[12, 15] = 1;

            field[13, 4] = 1;
            field[13, 6] = 1;
            field[13, 7] = 1;
            field[13, 8] = 1;
            field[13, 9] = 1;
            field[13, 11] = 1;
            field[13, 13] = 1;
            field[13, 15] = 1;
            field[13, 17] = 1;
            field[13, 18] = 1;

            field[14, 1] = 1;
            field[14, 2] = 1;
            field[14, 4] = 1;
            field[14, 6] = 1;
            field[14, 11] = 1;
            field[14, 17] = 1;


            field[15, 2] = 1;
            field[15, 4] = 1;
            field[15, 6] = 1;
            field[15, 7] = 1;
            field[15, 9] = 1;
            field[15, 11] = 1;
            field[15, 12] = 1;
            field[15, 14] = 1;
            field[15, 15] = 1;
            field[15, 16] = 1;
            field[15, 17] = 1;

            field[16, 4] = 1;
            field[16, 9] = 1;
            field[16, 12] = 1;

            field[17, 1] = 1;
            field[17, 2] = 1;
            field[17, 4] = 1;
            field[17, 5] = 1;
            field[17, 6] = 1;
            field[17, 7] = 1;
            field[17, 8] = 1;
            field[17, 9] = 1;
            field[17, 10] = 1;
            field[17, 11] = 1;
            field[17, 12] = 1;
            field[17, 13] = 1;
            field[17, 14] = 1;
            field[17, 15] = 1;
            field[17, 16] = 1;
            field[17, 17] = 1;

            field[18, 17] = 1;

            field[1, 3] = 2;
            field[1, 13] = 2;
            field[1, 18] = 2;
            field[7, 10] = 2;
            field[9, 9] = 2;
            field[9, 10] = 2;
            field[10, 9] = 2;
            field[10, 10] = 2;
            field[10, 16] = 2;
            field[13, 5] = 2;
            field[14, 7] = 2;
            field[14, 18] = 2;
            field[15, 1] = 2;
            field[16, 11] = 2;
            field[18, 1] = 2;
            field[18, 16] = 2;
            field[18, 18] = 2;


            tableLayoutPanel1.RowStyles.Clear();
            tableLayoutPanel1.ColumnStyles.Clear();
            tableLayoutPanel1.RowCount = n;
            tableLayoutPanel1.ColumnCount = m;

            for (int i = 0; i < n; i++)
                tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 1.0f / n));

            for (int j = 0; j < m; j++)
                tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 1.0f / m));

            for (int i = 0; i < n; i++) {

                for (int j = 0; j < m; j++) {
                    Panel panel = new Panel();
                    panel.Margin = new Padding(0);
                    tableLayoutPanel1.Controls.Add(panel, j, i);
                }

            }

            draw();

            timer1.Start();
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            if (playerPosI == 0)
                return;

            if (field[playerPosI - 1, playerPosJ] != 1)
                playerPosI--;

            if (field[playerPosI, playerPosJ] == 2)
            {
                score++;
                field[playerPosI, playerPosJ] = 0;
            }

            draw();
        }

        private void leftButton_Click(object sender, EventArgs e)
        {
            if (playerPosJ == 0)
                return;

            if (field[playerPosI, playerPosJ - 1] != 1)
                playerPosJ--;

           
            if (field[playerPosI, playerPosJ] == 2)
            {
                score++;
                field[playerPosI, playerPosJ] = 0;
            }

            draw();
        }

        private void downButton_Click(object sender, EventArgs e)
        {
            if (playerPosI == n - 1)
                return;

            if (field[playerPosI + 1, playerPosJ] != 1)
                playerPosI++;

            if (field[playerPosI, playerPosJ] == 2)
            {
                score++;
                field[playerPosI, playerPosJ] = 0;
            }

            draw();
        }

        private void rightButton_Click(object sender, EventArgs e)
        {
            if (playerPosJ == m - 1)
                return;

            if (field[playerPosI, playerPosJ + 1] != 1)
                playerPosJ++;


            if (field[playerPosI, playerPosJ] == 2)
            {
                score++;
                field[playerPosI, playerPosJ] = 0;
            }

            draw();

            if (playerPosJ == 19)
            {
                timer1.Stop();
                MessageBox.Show("Ваш счёт: " + score + "\nЗатраченое время: " + currentTime);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            currentTime += 1;
            timeLabel.Text = "Оставшееся время: " + (gameTime - currentTime) + " сек";

            if (gameTime - currentTime == 0) {
                timer1.Stop();
                MessageBox.Show("Вы проиграли");
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void scoreLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
